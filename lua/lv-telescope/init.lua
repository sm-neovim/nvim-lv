local actions = require('telescope.actions')
local trouble = require("trouble.providers.telescope")
local utils = require('telescope.utils')
-- Global remapping
------------------------------
-- '--color=never',
require('telescope').setup {
    defaults = {
        find_command = {'rg', '--no-heading', '--with-filename', '--line-number', '--column', '--smart-case'},
        prompt_prefix = " ",
        selection_caret = " ",
        entry_prefix = "  ",
        initial_mode = "insert",
        selection_strategy = "reset",
        sorting_strategy = "descending",
        layout_strategy = "horizontal",
        layout_config = {horizontal = {mirror = false}, vertical = {mirror = false}},
        file_sorter = require'telescope.sorters'.get_fzy_sorter,
        file_ignore_patterns = {},
        generic_sorter = require'telescope.sorters'.get_generic_fuzzy_sorter,
        winblend = 0,
        border = {},
        borderchars = {'─', '│', '─', '│', '╭', '╮', '╯', '╰'},
        color_devicons = true,
        use_less = true,
        set_env = {['COLORTERM'] = 'truecolor'}, -- default = nil,
        file_previewer = require'telescope.previewers'.vim_buffer_cat.new,
        grep_previewer = require'telescope.previewers'.vim_buffer_vimgrep.new,
        qflist_previewer = require'telescope.previewers'.vim_buffer_qflist.new,

        -- Developer configurations: Not meant for general override
        buffer_previewer_maker = require'telescope.previewers'.buffer_previewer_maker,
        mappings = {
            i = {
                ["<C-c>"] = actions.close,
                ["<C-j>"] = actions.move_selection_next,
                ["<C-k>"] = actions.move_selection_previous,
                ["<c-t>"] = trouble.open_with_trouble,
                ["<C-q>"] = actions.smart_send_to_qflist + actions.open_qflist,
                -- To disable a keymap, put [map] = false
                -- So, to not map "<C-n>", just put
                -- ["<c-x>"] = false,
                -- ["<esc>"] = actions.close,

                -- Otherwise, just set the mapping to the function that you want it to be.
                -- ["<C-i>"] = actions.select_horizontal,

                -- Add up multiple actions
                ["<CR>"] = actions.select_default + actions.center

                -- You can perform as many actions in a row as you like
                -- ["<CR>"] = actions.select_default + actions.center + my_cool_custom_action,
            },
            n = {
                ["<C-j>"] = actions.move_selection_next,
                ["<C-k>"] = actions.move_selection_previous,
                ["<c-t>"] = trouble.open_with_trouble,
                ["<C-q>"] = actions.smart_send_to_qflist + actions.open_qflist
                -- ["<C-i>"] = my_cool_custom_action,
            }
        }
    },
        fzy_native = {
            override_generic_sorter = false,
            override_file_sorter = true
        }
}

require('telescope').load_extension('fzy_native')
--require'telescope'.load_extension('project')

local M = {}

function M.grep_prompt()
    require('telescope.builtin').grep_string {
        search = vim.fn.input('Rg ')
    }
end

M.project_files = function()
    local _, ret, stderr = utils.get_os_command_output({
        'git', 'rev-parse', '--is-inside-work-tree'
    })
    local gopts = {}
    gopts.prompt_title = ' Git Files'
    gopts.prompt_prefix = '  '
    if ret == 0 then
        require'telescope.builtin'.git_files(gopts)
    else
        require'telescope.builtin'.find_files()
    end
end

-- @TODOUA: work HOME dot files into one of these
function M.grep_notes()
    local opts = {}
    opts.hidden = true
    opts.search_dirs = {
        '~/Sync/diary/', '~/.dotfiles/doc', '~/.config/nvim',
        '~/.config/bspwm', '~/.config/kitty', '~/.config/alacritty'
    }
    opts.prompt_prefix = '   '
    opts.prompt_title = ' Grep Notes'
    require'telescope.builtin'.live_grep(opts)
end

function M.find_notes()
    require('telescope.builtin').find_files {
        prompt_title = ' Find Notes',
        cwd = '~/Sync/smTaskFiles/',
        layout_strategy = 'horizontal',
        layout_config = {preview_width = 0.65}
    }
end

function M.browse_notes()
    require('telescope.builtin').file_browser {
        prompt_title = ' Browse Notes',
        prompt_prefix = ' ﮷ ',
        cwd = '~/Sync/smTaskFiles/',
        layout_strategy = 'horizontal',
        layout_config = {preview_width = 0.65}
    }
end

function M.find_config()
    require('telescope.builtin').find_files {
        prompt_title = ' NVim & Term Config Find',
        search_dirs = {
            '~/.dotfiles',
        },
        cwd = '~/.dotfiles/',
        layout_strategy = 'horizontal',
        layout_config = {preview_width = 0.65}
    }
end

function M.nvim_config()
    require('telescope.builtin').file_browser {
        prompt_title = ' NVim Config Browse',
        search_dirs = {
            '~/.config/nvim',
        },
        cwd = '~/.config/nvim/',
        layout_strategy = 'horizontal',
        layout_config = {preview_width = 0.65}
    }
end

function M.edit_zsh()
    require('telescope.builtin').find_files {
        prompt_title = '  zsh Config Find',
        search_dirs = {
            '~/.dotfiles/zinit', '~/.dotfiles/zsh-plug-funcs', '~/.dotfiles/zsh.links'
        },
        cwd = '~/.dotfiles/',
        layout_strategy = 'horizontal',
        layout_config = {preview_width = 0.75}
    }
end

function M.file_explorer()
    require('telescope.builtin').file_browser {
        prompt_title = ' File Browser',
        cwd = '~',
        layout_strategy = 'horizontal',
        layout_config = {preview_width = 0.65}
    }
end

function M.buffers()
  require("telescope.builtin").buffers()
end

function M.cur_buf()
  local opts = require('telescope.themes').get_dropdown {
    prompt_title = ' Search CurBuff',
    winblend = 10,
    border = true,
    previewer = false,
  }
  require('telescope.builtin').current_buffer_fuzzy_find(opts)
end

function M.fd()
  require("telescope.builtin").fd()
end

function M.builtin()
  require("telescope.builtin").builtin()
end

function M.git_status()
  local opts = require('telescope.themes').get_dropdown {
    prompt_title = ' Git Status',
    prompt_prefix = '  ',
    winblend = 10,
    border = true,
    previewer = false,
    layout_config = { width = 0.65 },
  }

   -- Can change the git icons using this.
  -- opts.git_icons = {
  --   changed = "M"
  -- }

  require("telescope.builtin").git_status(opts)
end

function M.git_commits()
  require("telescope.builtin").git_commits {
    prompt_title = ' Git Commits',
    prompt_prefix = '  ',
    winblend = 5,
    layout_strategy = "horizontal",
    layout_config = { preview_width = 0.65, width = 0.85 },
  }
end

function M.search_only_certain_files()
  require("telescope.builtin").find_files {
    find_command = {
      "rg",
      "--files",
      "--type",
      vim.fn.input "Type: ",
    },
  }
end

return M

