local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath("data") .. "/site/pack/packer/opt/packer.nvim"
local start_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"

if fn.empty(fn.glob(install_path)) > 0 then
    execute("!git clone https://github.com/wbthomason/packer.nvim " .. start_path)
    execute "packadd packer.nvim"
end
vim.cmd [[packadd packer.nvim]]

--- Check if a file or directory exists in this path
local function require_plugin(plugin)
    local plugin_prefix = fn.stdpath("data") .. "/site/pack/packer/opt/"

    local plugin_path = plugin_prefix .. plugin .. "/"
    --	print('test '..plugin_path)
    local ok, err, code = os.rename(plugin_path, plugin_path)
    if not ok then
        if code == 13 then
            -- Permission denied, but it exists
            return true
        end
    end
    --	print(ok, err, code)
    if ok then vim.cmd("packadd " .. plugin) end
    return ok, err, code
end

local packer = require 'packer'
-- Why do this? https://dev.to/creativenull/installing-neovim-nightly-alongside-stable-10d0
-- A little more context, if you have a different location for your init.lua outside ~/.config/nvim
-- in my case, this is ~/.config/nvim-nightly - you may want to set a custom location for packer
packer.init {
  package_root = os.getenv('HOME') .. '/.local/share/nvim/site/pack',
  compile_path = os.getenv('HOME') .. '/.cache/lv-nvim/plugin/packer_compiled.vim'
}

vim.cmd "autocmd BufWritePost plugins.lua PackerCompile" -- Auto compile when there are changes in plugins.lua

--return require("packer").startup(function(use)
packer.startup(function(use)
    -- Packer can manage itself as an optional plugin
    use {"wbthomason/packer.nvim",
            opt = true}


        -- TODO refactor all of this (for now it works, but yes I know it could be wrapped in a simpler function)
--        use {"neovim/nvim-lspconfig", opt = true}
        use {"neovim/nvim-lspconfig",
                event = "BufRead",
                opt = true}
        use {"kabouzeid/nvim-lspinstall", opt = true}
        use {"glepnir/lspsaga.nvim", opt = true}
        use {"folke/trouble.nvim", opt = true}

        -- Telescope
        -- Find, Filter, Preview, Pick. All lua, all the time.
        use {
          "nvim-telescope/telescope.nvim",
          opt = true,
          requires = {
            -- An implementation of the Popup API from vim in Neovim.
            { "nvim-lua/popup.nvim",
    			opt=true},
            -- plenary: full; complete; entire; absolute; unqualified.
            { "nvim-lua/plenary.nvim",
    			opt=true},
            -- fzy: use fast fzy algorithm.
            {'nvim-telescope/telescope-fzy-native.nvim',
                opt = true},
            -- project: use .
--            {'nvim-telescope/telescope-project.nvim',
--                opt = true},
          },
        }

        -- Teesitter
        use {
          "nvim-treesitter/nvim-treesitter",
            run = ":TSUpdate",
          requires = {
            {"windwp/nvim-ts-autotag",
                after = { 'nvim-treesitter' },
                opt = true},
            {'andymass/vim-matchup',
                after = { 'nvim-treesitter' },
                opt = true},
          },
        }

        -- Dbugging
        use {"mfussenegger/nvim-dap", opt = true}

        -- Atocomplete
--        use {"hrsh7th/nvim-compe",
--                event = "InsertEnter",
--                opt = true}
        use {
            "hrsh7th/nvim-cmp",
            requires = {
                "hrsh7th/cmp-buffer", "hrsh7th/cmp-nvim-lsp",
                'quangnguyen30192/cmp-nvim-ultisnips', 'hrsh7th/cmp-nvim-lua',
                'octaltree/cmp-look', 'hrsh7th/cmp-path', 'hrsh7th/cmp-calc',
                'f3fora/cmp-spell', 'hrsh7th/cmp-emoji'
            }
        }
        use {
            'tzachar/cmp-tabnine',
            run = './install.sh',
            requires = 'hrsh7th/nvim-cmp'
        }
        use {
            'onsails/lspkind-nvim'
        }
        use {"hrsh7th/vim-vsnip",
                event = "InsertEnter",
                opt = true}
        use {"rafamadriz/friendly-snippets",
                event = "InsertEnter",
                opt = true}
       use {"ray-x/lsp_signature.nvim",
                opt = true}

        -- Eplorer
        use {"kyazdani42/nvim-tree.lua", opt=true}
        use {"ahmedkhalf/lsp-rooter.nvim", opt = true} -- with this nvim-tree will follow you
        -- TODO remove when open on dir is supported by nvimtree
        use "kevinhwang91/rnvimr"

        -- use {'lukas-reineke/indent-blankline.nvim', opt=true, branch = 'lua'}
        use {"lewis6991/gitsigns.nvim", event = "BufRead", opt = true}
        -- Packer
        use {'sindrets/diffview.nvim', event = "BufRead", opt = true}

        use {"folke/which-key.nvim", opt = true}
--        use {"ChristianChiarulli/dashboard-nvim",
--                event = 'BufWinEnter',
--                opt = true}
        use {'mhinz/vim-startify', opt = true}
        use {"windwp/nvim-autopairs",
                event = "InsertEnter",
                opt = true}
        use {"terrortylor/nvim-comment", opt = true}
--        use {"kevinhwang91/nvim-bqf", opt = true}
           -- better quickfix
           use { 'kevinhwang91/nvim-bqf',
                opt = true,
                ft = { 'qf' } }

        -- Color
        use {"christianchiarulli/nvcode-color-schemes.vim",
                setup = function() vim.g.nvcode_termcolors = 256 end,
                opt = true}
		use {'norcalli/nvim-colorizer.lua',
                cmd = {'ColorizerAttachToBuffer', 'ColorizerDetachFromBuffer' },
                opt=true}

        -- Icons
--        use {"kyazdani42/nvim-web-devicons", opt = true}
           -- lua `fork` of vim-web-devicons for neovim
        use {
          "kyazdani42/nvim-web-devicons",
          opt = true,
          requires = {
            -- Icon set using nonicons for neovim plugins and settings.
            -- requires nonicons font installed
            "yamatsum/nvim-nonicons",
          opt = true,
          }
        }

        -- Status Line and Bufferline
        use {"glepnir/galaxyline.nvim", opt = true}
--        use {"romgrk/barbar.nvim", opt = true}
        use {
        "romgrk/barbar.nvim",
            config = function()
                vim.api.nvim_set_keymap('n', '<TAB>', ':BufferNext<CR>',
                                        {noremap = true, silent = true})
                vim.api.nvim_set_keymap('n', '<S-TAB>', ':BufferPrevious<CR>',
                                        {noremap = true, silent = true})
                vim.api.nvim_set_keymap('n', '<S-x>', ':BufferClose<CR>',
                                        {noremap = true, silent = true})
            end,
            event = "BufRead",
            opt = true
        }

		-- float term
		use {
			'voldikss/vim-floaterm',
                event = "BufRead",
			    opt=true
			}

         -- Zen Mode
        -- use {"Pocco81/TrueZen.nvim", opt = true}

		-- Sane gx for netrw_gx bug
        use {"felipec/vim-sanegx", event = "BufRead", opt = true}


    -- load plugins
		require_plugin("vim-sanegx")
		require_plugin("nvim-lspconfig")
		require_plugin("lspsaga.nvim")
		require_plugin("nvim-lspinstall")
        require_plugin("lsp_signature.nvim")
		require_plugin('trouble.nvim')
		require_plugin("friendly-snippets")
		require_plugin("popup.nvim")
		require_plugin("plenary.nvim")
		require_plugin("telescope-fzy-native.nvim")
		require_plugin("telescope.nvim")
--        require_plugin('telescope-project.nvim')
		require_plugin("nvim-dap")
--		require_plugin("nvim-compe")
		require_plugin("vim-vsnip")
		require_plugin("nvim-treesitter")
		require_plugin("nvim-ts-autotag")
        require_plugin('vim-matchup')
		require_plugin("nvim-tree.lua")
		require_plugin("neogit")
		require_plugin("gitsigns.nvim")
		require_plugin("diffview.nvim")
		require_plugin("which-key.nvim")
--		require_plugin("dashboard-nvim")
		require_plugin("vim-startify")
		require_plugin("nvim-autopairs")
		require_plugin("nvim-comment")
		require_plugin("nvim-bqf")
		require_plugin("nvcode-color-schemes.vim")
		require_plugin("nvim-web-devicons")
		require_plugin("nvim-nonicons")
		require_plugin("nvim-colorizer.lua")
		require_plugin("galaxyline.nvim")
		require_plugin("barbar.nvim")
        require_plugin('lsp-rooter.nvim')
		require_plugin("vim-floaterm")
        require_plugin("TrueZen.nvim")

    -- Extras

end)
