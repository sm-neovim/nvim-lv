require("which-key").setup {
    plugins = {
        marks = true, -- shows a list of your marks on ' and `
        registers = true, -- shows your registers on " in NORMAL or <C-r> in INSERT mode
        -- the presets plugin, adds help for a bunch of default keybindings in Neovim
        -- No actual key bindings are created
        spelling = {
          enabled = true, -- enabling this will show WhichKey when pressing z= to select spelling suggestions
          suggestions = 20, -- how many suggestions should be shown in the list?
        },
        presets = {
            operators = true, -- adds help for operators like d, y, ...
            motions = true, -- adds help for motions
            text_objects = true, -- help for text objects triggered after entering an operator
            windows = true, -- default bindings on <c-w>
            nav = true, -- misc bindings to work with windows
            z = true, -- bindings for folds, spelling and others prefixed with z
            g = true -- bindings for prefixed with g
        }
    },
    icons = {
        breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
        separator = "➜", -- symbol used between a key and it's label
        group = "+" -- symbol prepended to a group
    },
    window = {
        border = "single", -- none, single, double, shadow
        position = "bottom", -- bottom, top
        margin = {1, 0, 1, 0}, -- extra window margin [top, right, bottom, left]
        padding = {2, 2, 2, 2} -- extra window padding [top, right, bottom, left]
    },
    layout = {
        height = {min = 4, max = 25}, -- min and max height of the columns
        width = {min = 20, max = 50}, -- min and max width of the columns
        spacing = 3, -- spacing between columns
        align = "left", -- align columns left, center or right
    },
    hidden = {"<silent>", "<cmd>", "<Cmd>", "<CR>", "call", "lua", "^:", "^ "}, -- hide mapping boilerplate
    show_help = true -- show help message on the command line when the popup is visible
}

local opts = {
    mode = "n", -- NORMAL mode
    prefix = "<leader>",
    buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
    silent = true, -- use `silent` when creating keymaps
    noremap = true, -- use `noremap` when creating keymaps
    nowait = false -- use `nowait` when creating keymaps
}

-- Set leader
vim.api.nvim_set_keymap('n', '<Space>', '<NOP>', {noremap = true, silent = true})
vim.g.mapleader = ' '

-- no hl
vim.api.nvim_set_keymap('n', '<Leader>h', ':set hlsearch!<CR>', {noremap = true, silent = true})

-- explorer
vim.api.nvim_set_keymap('n', '<Leader>e', ':NvimTreeToggle<CR>', {noremap = true, silent = true})

-- telescope
vim.api.nvim_set_keymap('n', '<Leader>f', ':Telescope find_files<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<Leader>b', ':Telescope buffers<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<Leader>t', ':Telescope <CR>', {noremap = true, silent = true})

-- dashboard
--vim.api.nvim_set_keymap('n', '<Leader>;', ':Dashboard<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<Leader>;', ':Startify<CR>', {noremap = true, silent = true})

-- Comments
vim.api.nvim_set_keymap("n", "<leader>/", ":CommentToggle<CR>", {noremap = true, silent = true})
vim.api.nvim_set_keymap("v", "<leader>/", ":CommentToggle<CR>", {noremap = true, silent = true})

-- close buffer
vim.api.nvim_set_keymap("n", "<leader>c", ":BufferClose<CR>", {noremap = true, silent = true})

-- vifm
vim.api.nvim_set_keymap("n", "<leader>V", ":FloatermNew vifm<CR>", {noremap = true, silent = true})
-- lf
vim.api.nvim_set_keymap("n", "<leader>L", ":FloatermNew lf<CR>", {noremap = true, silent = true})
-- ranger
vim.api.nvim_set_keymap("n", "<leader>R", ":RnvimrToggle<CR>", {noremap = true, silent = true})

-- open projects
--vim.api.nvim_set_keymap('n', '<leader>P', ":lua require'telescope'.extensions.project.project{}<CR>",
--                        {noremap = true, silent = true})
-- TODO create entire treesitter section

local mappings = {
    ["/"] = "Comment",
    ["c"] = "Close Buffer",
    ["e"] = "Explorer",
    ["f"] = "Find File",
    ["h"] = "No Highlight",
    d = {
        name = "+Diagnostics",
        t = {"<cmd>TroubleToggle<cr>", "trouble"},
        w = {"<cmd>TroubleToggle lsp_workspace_diagnostics<cr>", "workspace"},
        d = {"<cmd>TroubleToggle lsp_document_diagnostics<cr>", "document"},
        q = {"<cmd>TroubleToggle quickfix<cr>", "quickfix"},
        l = {"<cmd>TroubleToggle loclist<cr>", "loclist"},
        r = {"<cmd>TroubleToggle lsp_references<cr>", "references"},
    },
    D = {
        name = "+Debug",
        b = {"<cmd>DebugToggleBreakpoint<cr>", "Toggle Breakpoint"},
        c = {"<cmd>DebugContinue<cr>", "Continue"},
        i = {"<cmd>DebugStepInto<cr>", "Step Into"},
        o = {"<cmd>DebugStepOver<cr>", "Step Over"},
        r = {"<cmd>DebugToggleRepl<cr>", "Toggle Repl"},
        s = {"<cmd>DebugStart<cr>", "Start"}
    },
    g = {
        name = "+Git",
        j = {"<cmd>NextHunk<cr>", "Next Hunk"},
        k = {"<cmd>PrevHunk<cr>", "Prev Hunk"},
        p = {"<cmd>PreviewHunk<cr>", "Preview Hunk"},
        r = {"<cmd>ResetHunk<cr>", "Reset Hunk"},
        R = {"<cmd>ResetBuffer<cr>", "Reset Buffer"},
        s = {"<cmd>StageHunk<cr>", "Stage Hunk"},
        u = {"<cmd>UndoStageHunk<cr>", "Undo Stage Hunk"},
        o = {"<cmd>Telescope git_status<cr>", "Open changed file"},
        b = {"<cmd>Telescope git_branches<cr>", "Checkout branch"},
        c = {"<cmd>Telescope git_commits<cr>", "Checkout commit"},
        C = {"<cmd>Telescope git_bcommits<cr>", "Checkout commit(for current file)"},
        D = {
            name = "+DiffView",
            c = {"<cmd>DiffviewClose<cr>", "Close Diff View git"},
            f = {"<cmd>DiffviewFocusFiles<cr>", "Bring focus to the files panel"},
            o = {"<cmd>DiffviewOpen<cr>", "Open Diff View git"},
            r = {"<cmd>DiffviewRefresh<cr>", "Update stats and entries"},
            t = {"<cmd>DiffviewToggleFiles<cr>", "Toggle the files panel"},
        }
    },
    l = {
        name = "+LSP",
        a = {"<cmd>Lspsaga code_action<cr>", "Code Action"},
        A = {"<cmd>Lspsaga range_code_action<cr>", "Selected Action"},
        d = {"<cmd>Telescope lsp_document_diagnostics<cr>", "Document Diagnostics"},
        D = {"<cmd>Telescope lsp_workspace_diagnostics<cr>", "Workspace Diagnostics"},
        f = {"<cmd>LspFormatting<cr>", "Format"},
        i = {"<cmd>LspInfo<cr>", "Info"},
        l = {"<cmd>Lspsaga lsp_finder<cr>", "LSP Finder"},
        L = {"<cmd>Lspsaga show_line_diagnostics<cr>", "Line Diagnostics"},
        p = {"<cmd>Lspsaga preview_definition<cr>", "Preview Definition"},
        q = {"<cmd>Telescope quickfix<cr>", "Quickfix"},
        r = {"<cmd>Lspsaga rename<cr>", "Rename"},
        t = {"<cmd>LspTypeDefinition<cr>", "Type Definition"},
        x = {"<cmd>cclose<cr>", "Close Quickfix"},
        s = {"<cmd>Telescope lsp_document_symbols<cr>", "Document Symbols"},
        S = {"<cmd>Telescope lsp_workspace_symbols<cr>", "Workspace Symbols"}
    },

    s = {
        name = "+Search",
        b = {"<cmd>Telescope git_branches<cr>", "git-branches"},
        c = {"<cmd>Telescope colorscheme<cr>", "Colorscheme"},
        d = {"<cmd>Telescope lsp_document_diagnostics<cr>", "Document Diagnostics"},
        D = {"<cmd>Telescope lsp_workspace_diagnostics<cr>", "Workspace Diagnostics"},
        f = {"<cmd>Telescope find_files<cr>", "Find File"},
        F = {"<cmd>Telescope file_browser<cr>", "File Browser"},
        m = {"<cmd>Telescope marks<cr>", "Marks"},
        M = {"<cmd>Telescope man_pages<cr>", "Man Pages"},
        o = {"<cmd>Telescope oldfiles<cr>", "Open Recent File"},
        r = {"<cmd>Telescope registers<cr>", "Registers"},
        t = {"<cmd>Telescope live_grep<cr>", "Text"}
    },
    S = {name = "+Session", s = {"<cmd>SessionSave<cr>", "Save Session"}, l = {"<cmd>SessionLoad<cr>", "Load Session"}},

    -- extras
    T = {
        name = "+TelescopeFuncs",
        B = {"<cmd>lua require\'lv-telescope\'.builtin()<cr>", "Builtins Telescope"},
        b = {"<cmd>lua require\'lv-telescope\'.buffers()<cr>", "Buffers Telescope"},
        c = {"<cmd>lua require\'lv-telescope\'.cur_buf()<cr>", "Curr Buff Telescope"},
        d = {"<cmd>lua require\'lv-telescope\'.fd()<cr>", "Find Dir Telescope"},
        e = {"<cmd>lua require\'lv-telescope\'.file_explorer()<cr>", "File Explorer Telescope"},
        f = {"<cmd>lua require\'lv-telescope\'.find_config()<cr>", "Find Config Telescope"},
        g = {"<cmd>lua require\'lv-telescope\'.git_status()<cr>", "Git Status Telescope"},
        G = {"<cmd>lua require\'lv-telescope\'.git_commits()<cr>", "Git Commits Telescope"},
        n = {"<cmd>lua require\'lv-telescope\'.nvim_config()<cr>", "Nvim Config Telescope"},
        o = {"<cmd>lua require\'lv-telescope\'.search_only_certain_files()<cr>", "Search Only Certain FileTypes"},
        p = {"<cmd>lua require\'lv-telescope\'.project_files()<cr>", "Project Files Telescope"},
        r = {"<cmd>lua require\'lv-telescope\'.grep_prompt()<cr>", "Rg Prompt Telescope"},
        t = {"<cmd>lua require\'lv-telescope\'.browse_notes()<cr>", "Notes Brose Telescope"},
        w = {"<cmd>lua require\'lv-telescope\'.find_notes()<cr>", "Notes Find Telescope"},
        W = {"<cmd>lua require\'lv-telescope\'.grep_notes()<cr>", "Notes Grep Telescope"},
        z = {"<cmd>lua require\'lv-telescope\'.edit_zsh()<cr>", "Edit ZSH Config"},
    },
    z = {
        name = "+Zen",
        s = {"<cmd>TZBottom<cr>", "toggle status line"},
        t = {"<cmd>TZTop<cr>", "toggle tab bar"},
        z = {"<cmd>TZAtaraxis<cr>", "toggle zen"},
    }
}

local wk = require("which-key")
-- x is for the terminal
wk.register({
  ["<leader>x"] = { name = "+terminal" },
  ["<leader>xf"] = { ":FloatermNew fzf<CR>", "fzf" },
  ["<leader>xg"] = { ":FloatermNew lazygit<CR>", "lazy git" },
  ["<leader>xn"] = { ":FloatermNew node<CR>", "node" },
  ["<leader>xp"] = { ":FloatermNew ipython3<CR>", "python" },
  ["<leader>xr"] = { ":FloatermNew ranger<CR>", "ranger" },
  ["<leader>xh"] = { ":FloatermNew htop<CR>", "htop" },
})
-- f is for the files
wk.register({
  ["<leader>F"] = {name = "+telefiles"},
  ["<leader>Ff"] = { ":lua require('telescope.builtin').find_files()<CR>", "Telescope Find Files"},
  ["<leader>Fo"] = { ":lua require('telescope.builtin').oldfiles()<CR>", "Telescope Recents"},
  ["<leader>Fw"] = { ":Telescope live_grep<CR>", "Telescope Find Word"},
  ["<leader>Fh"] = { ":lua require('telescope.builtin').help_tags()<CR>]", "Help Tags"},
  ["<leader>Fb"] = { ":lua require('telescope.builtin').buffers()<CR>]", "Telescope buffers"},
  ["<leader>Fr"] = { ":lua require('telescope.builtin').registers()<CR>]", "Telescope registers"},
})

wk.register(mappings, opts)


