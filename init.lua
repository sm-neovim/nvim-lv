require('lv-globals')
vim.cmd('luafile '..CONFIG_PATH..'/lv-settings.lua')
require('settings')
require('lv-matchup')
require('plugins')
require('lv-utils')
require('lv-autocommands')
require('keymappings')
require('lv-nvimtree')
require('colorscheme') -- This plugin must be required somewhere after nvimtree. Placing it before will break navigation keymappings
require('lv-colorizer')
require('lv-galaxyline')
require('lv-comment')
require('lv-cmp')
--require('lv-compe')
--require('lv-dashboard')
require('lv-startify')
require('lv-telescope')
require('lv-gitsigns')
require('lv-diffview')
require('lv-treesitter')
require('lv-autopairs')
--require('lv-barbar')
require('lv-rnvimr')
require('lv-floaterm')
require('lv-which-key')
require('lv-lsp-rooter')
require('lv-lspsignature')
require('lv-lspkind')
--require('lv-zen')

-- Disable some built-in plugins we don't want
local disabled_built_ins = {
  'gzip', 'man', 'matchit', 'matchparen', 'shada_plugin', 'tarPlugin', 'tar', 'zipPlugin', 'zip',
  'netrwPlugin'
}
for i = 1, 10 do vim.g['loaded_' .. disabled_built_ins[i]] = 1 end


-- extras
if O.extras then
--    require('lv-numb')
--    require('lv-dial')
--    require('lv-hop')
--    require('lv-symbols-outline')
end

-- TODO is there a way to do this without vimscript
vim.cmd('source '..CONFIG_PATH..'/vimscript/functions.vim')

-- LSP
require('lsp')
require('lsp.bash-ls')
require('lsp.clangd')
require('lsp.css-ls')
require('lsp.emmet-ls')
require('lsp.efm-general-ls')
require('lsp.go-ls')
require('lsp.graphql-ls')
require('lsp.html-ls')
require('lsp.js-ts-ls')
require('lsp.json-ls')
require('lsp.latex-ls')
require('lsp.lua-ls')
require('lsp.php-ls')
require('lsp.python-ls')
require('lsp.rust-ls')
require('lsp.ruby-ls')
require('lsp.svelte-ls')
require('lsp.terraform-ls')
require('lsp.vim-ls')
require('lsp.yaml-ls')

